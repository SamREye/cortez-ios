// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

final class BackupPhraseViewLayout: UICollectionViewFlowLayout {
	
	override init() {
		super.init()
		setup()
	}
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		minimumInteritemSpacing = 12
		minimumLineSpacing = 12
	}
	
	override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		let attributes = super.layoutAttributesForElements(in: rect)?.compactMap { $0.copy() as? UICollectionViewLayoutAttributes }
		attributes?.forEach {
			if $0.representedElementKind == nil {
				let indexPath = $0.indexPath
				$0.frame = layoutAttributesForItem(at: indexPath)?.frame ?? .zero
			}
		}
		return attributes
	}
	
	override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		guard let collectionView = collectionView else { return nil }
		guard let attributes = super.layoutAttributesForItem(at: indexPath)?.copy() as? UICollectionViewLayoutAttributes else {
			return nil
		}

		if indexPath.item == 0 { // first item of section
			var frame = attributes.frame
			frame.origin.x = sectionInset.left // first item of the section should always be left aligned
			attributes.frame = frame
			
			return attributes
		}
		
		let previousIndexPath = IndexPath(item: indexPath.item - 1, section: indexPath.section)
		let previousFrame = layoutAttributesForItem(at: previousIndexPath)?.frame ?? .zero
		let previousFrameRightPoint = previousFrame.origin.x + previousFrame.size.width + minimumInteritemSpacing
		
		let currentFrame = attributes.frame
		let strecthedCurrentFrame = CGRect(
			x: 0, y: currentFrame.origin.y,
			width: collectionView.frame.size.width, height: currentFrame.size.height
		)
		
		if !previousFrame.intersects(strecthedCurrentFrame) {
			// if current item is the first item on the line
			// the approach here is to take the current frame, left align it to the edge of the view
			// then stretch it the width of the collection view, if it intersects with the previous frame then that means it
			// is on the same line, otherwise it is on it's own new line
			var frame = attributes.frame
			frame.origin.x = sectionInset.left // first item on the line should always be left aligned
			attributes.frame = frame
			return attributes
		}

		var frame = attributes.frame
		frame.origin.x = previousFrameRightPoint
		attributes.frame = frame
		return attributes
	}
}
