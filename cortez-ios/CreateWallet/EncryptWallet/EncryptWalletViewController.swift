// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol EncryptWalletDisplayLogic: class {
	func show(_ viewModel: Wallet.ViewModel)
}

class EncryptWalletViewController: UIViewController, OnboardPageViewController, PasswordControlDelegate {

	var router: (OnboardPageRoutingLogic & OnboardPageDataPassingLogic)?
	var interactor: EncryptWalletBusinessLogic?

	@IBOutlet private weak var passwordField: UITextField!
	@IBOutlet private weak var passwordConfirmationField: UITextField!

	@IBOutlet private weak var passworControl: PasswordControl!

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = EncryptWalletRouter()
		let interactor = EncryptWalletInteractor(creator: WalletCreator(), presenter: EncryptWalletPresenter(display: self))
		router.dataStore = interactor
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		passwordField.rightView = UIImageView(image: UIImage(named: "failure-24px"))
		passwordField.rightView?.tintColor = UIColor(named: "text")
		passwordField.rightViewMode = .always
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		passwordControlStateDidChange(passworControl)
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		// UIPageViewController scroll view issue during page transition
		UIView.animate(withDuration: -1, animations: {}) { _ in
			self.passwordField.becomeFirstResponder()
		}
	}

	func encryptWallet() {
		guard let password = passwordField.text else { return }
		interactor?.execute(.init(password: password))
	}
	
	// MARK: - PasswordControlDelegate

	func passwordControlStateDidChange(_ passwordControl: PasswordControl) {
		onboardController?.isNextButtonEnabled = passwordControl.state == [.validPassword, .confirmedPassword]
	}

	func passwordControlPasswordConfirmationFieldDidReturn(_ passwordControl: PasswordControl) {
		encryptWallet()
	}
	
	// MARK: - OnboardPageViewController

	func complete() {
		encryptWallet()
	}
}

extension EncryptWalletViewController: EncryptWalletDisplayLogic {
	func show(_ viewModel: Wallet.ViewModel) {
		if let message = viewModel.message {
			let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
			present(alert, animated: true, completion: nil)
			return
		}
		dismiss(animated: true, completion: nil)
	}
}
