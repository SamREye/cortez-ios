// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

protocol RestoreWalletBusinessLogic {
	func execute(_ request: Wallet.BackupPhrase.WordSuggestions.Request)
	func execute(_ request: Wallet.BackupPhrase.Validation.Request)
}

protocol RestoreWalletDataStore: OnboardPageDataStore {
	var backupPhrase: String? { get set }
}

final class RestoreWalletInteractor: RestoreWalletBusinessLogic, RestoreWalletDataStore {

	var backupPhrase: String?

	let suggester: BackupPhraseWordSuggestionLogic
	let validator: BackupPhraseValidationLogic
	let presenter: RestoreWalletPresentationLogic

	init(suggester: BackupPhraseWordSuggestionLogic, validator: BackupPhraseValidationLogic, presenter: RestoreWalletPresentationLogic) {
		self.suggester = suggester
		self.validator = validator
		self.presenter = presenter
	}

	func execute(_ request: Wallet.BackupPhrase.WordSuggestions.Request) {
		suggester.suggestWords(forPrefix: request.prefix) {
			self.presenter.format(.init(words: $0))
		}
	}
	func execute(_ request: Wallet.BackupPhrase.Validation.Request) {
		let backupPhrase = request.backupPhrase
			.trimmingCharacters(in: .whitespacesAndNewlines)
			.components(separatedBy: .whitespacesAndNewlines)
			.joined(separator: " ")
		validator.validate(backupPhrase) { [weak self] in
			guard let `self` = self else { return }
			if $0 {
				self.backupPhrase = backupPhrase
			}
			self.presenter.format(.init(success: $0))
		}
	}
}
