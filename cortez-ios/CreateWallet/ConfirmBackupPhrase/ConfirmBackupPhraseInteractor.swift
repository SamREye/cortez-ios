// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

protocol ConfirmBackupPhraseBusinessLogic {
	func execute(_ request: Wallet.BackupPhrase.Validation.Request)
}

protocol ConfirmBackupPhraseDataStore: OnboardPageDataStore {
	var backupPhrase: String? { get set }
}

final class ConfirmBackupPhraseInteractor: ConfirmBackupPhraseBusinessLogic, ConfirmBackupPhraseDataStore {

	var backupPhrase: String? {
		didSet {
			debug("backup phrase: \(backupPhrase ?? "none")")
			presenter.format(.init(backupPhrase: backupPhrase))
		}
	}

	let comparator: BackupPhraseCompareLogic
	let presenter: ConfirmBackupPhrasePresentationLogic

	init(validator: BackupPhraseCompareLogic, presenter: ConfirmBackupPhrasePresentationLogic) {
		self.comparator = validator
		self.presenter = presenter
	}

	func execute(_ request: Wallet.BackupPhrase.Validation.Request) {
		guard let backupPhrase = backupPhrase else { return }
		comparator.compare(request.backupPhrase, to: backupPhrase) {
			self.presenter.format(.init(success: $0))
		}
	}
}
