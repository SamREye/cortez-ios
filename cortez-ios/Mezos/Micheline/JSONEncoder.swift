// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************


import Foundation

private protocol JSONEncodable {
	var encodable: Encodable { get }
}

extension Int64: JSONEncodable {
	var encodable: Encodable { "\(self)" }
}

extension String: JSONEncodable {
	var encodable: Encodable { self }
}

extension Primitive: Encodable {
	
	private enum CodingKeys: String, CodingKey {
		case name = "prim"
		case arguments = "args"
		case annotations = "annots"
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(name.rawValue, forKey: .name)
		try container.encodeIfPresent(arguments, forKey: .arguments)
		try container.encodeIfPresent(annotations, forKey: .annotations)
	}
}

private struct JSONFriendlyVisitable<Value: Visitable & JSONEncodable>: Visitable {
	
	let key: String
	let value: Value
	
	func accept(_ visitor: Visitor) throws {
		try value.accept(visitor)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: String.self)
		try container.encode(value.encodable, forKey: key)
	}
}

extension AnyVisitable {
	init(_ value: Int64) {
		self.init(JSONFriendlyVisitable(key: "int", value: value))
	}
	init(_ value: String) {
		self.init(JSONFriendlyVisitable(key: "string", value: value))
	}
}
