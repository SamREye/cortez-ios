// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import CoreImage
import UIKit

enum Transfer {

	case delegation(Delegation)
	case origination(Origination)
	case payment(Payment)

	enum QRCode {}
	enum Encoding {}
	enum Injection {}
}

extension Transfer {
	struct Delegation {
		var source: Account
		var delegate: Contact?
	}
	
	struct Origination {
		var name: String
		var amount: UInt64
		var delegate: Contact?
	}
	
	struct Payment {
		var amount: UInt64
		var source: Account
		var destination: Contact
	}
}

extension Transfer.QRCode {
	struct Request {}
	struct Response {
		let image: CIImage
	}
	struct ViewModel {
		let image: UIImage
	}
}

extension Transfer.Encoding {
	struct Request {}
	enum Response {
		case encoding(Transfer)
		case encoded(fees: UInt64)
		case failed(error: Error)
	}
	enum ViewModel {
		case encoding(question: NSAttributedString)
		case encoded(fees: NSAttributedString)
		case failed(message: String)
	}
}

extension Transfer.Injection {
	struct Request {}
	struct Response {
		let error: Error?
	}
	struct ViewModel {
		let message: String?
	}
}
