// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

enum TransferSegue: String {
	case request
	case pay
}

class TransferViewController: UIViewController {

	var router: (TransferRoutingLogic & TransferDataPassingLogic)?
	var interactor: (TransferBusinessLogic & TransferDataStore)?

	@IBOutlet weak var amountView: UITextView!
	
	@IBOutlet weak var decimalButton: UIButton!
	@IBOutlet weak var deleteButton: UIButton!
	
	@IBOutlet weak var requestButton: UIButton!
	@IBOutlet weak var payButton: UIButton!
	
	@IBOutlet weak var bottomConstraint: NSLayoutConstraint!
	
	@IBOutlet weak var tezPadControl: TezPadControl!
	
	var deleteTimer: Timer?
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = TransferRouter()
		let interactor = TransferInteractor()
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		amountView.inputView = UIView() // prevent keyboard from appearing
		
		decimalButton.setTitle(tezPadControl.formatter.decimalSeparator, for: .normal)
		
		updateAmountView()
		updateDecimalPadButtons()
		updateButtons()

		// UIKit bug
		let tintColor = amountView.tintColor
		amountView.tintColor = .clear
		amountView.tintColor = tintColor
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		guard amountView.alpha == 0 else { return }

		amountView.becomeFirstResponder()
		
		updateAmountViewFont()
		amountView.centerText()

		amountView.alpha = 0
		amountView.layer.setAffineTransform(.init(scaleX: 0, y: 0))
		UIView.animate(withDuration: -1) {
			self.amountView.alpha = 1
			self.amountView.layer.setAffineTransform(.identity)
		}

		
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		DispatchQueue.main.async {
			self.amountView.becomeFirstResponder()
			self.setAmountFieldCursorPosition()
		}
	}
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		deleteTimer?.invalidate()
	}
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		amountView.alpha = 0
	}

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
		guard let destination = segue.identifier.flatMap(TransferSegue.init) else { return }
		interactor?.amount = tezPadControl.value
		switch destination {
		case .request:
			router?.routeToRequest(with: segue)
		case .pay:
			router?.routeToPay(with: segue)
		}
    }
	
	@IBAction func unwind(segue: UIStoryboardSegue) {
		dismiss(animated: true, completion: nil)
	}
}

extension TransferViewController: TezPadControlDelegate {
	
	func tezPadControlDidUpdateAmount(_ tezPadControl: TezPadControl) {
		updateAmountView()
		updateDecimalPadButtons()
		updateButtons()
		amountView.setNeedsLayout()
		amountView.layoutIfNeeded()
	}
	
	func updateAmountViewFont() {
		while let font = amountView.font, Int(amountView.contentSize.height / font.lineHeight) == 1 && font.pointSize < 123 {
			amountView.font = UIFont(name: font.fontName, size: font.pointSize + 1)
		}
		while let font = amountView.font, Int(amountView.contentSize.height / font.lineHeight) > 1 && font.pointSize > 17 {
			amountView.font = UIFont(name: font.fontName, size: font.pointSize - 1)
		}
	}
	
	@IBAction func updateAmount(_ sender: UIButton) {
		guard let string = sender.titleLabel?.text else { return }
		tezPadControl.updateAmount(with: string)
	}
	
	@IBAction func beginDeletion() {
		tezPadControl.updateAmount(with: .empty)
		deleteTimer = .scheduledTimer(withTimeInterval: 0.3, repeats: false) { [weak self] _ in
			self?.deleteTimer = .scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
				self?.tezPadControl.updateAmount(with: .empty)
			}
		}
	}
	@IBAction func endDeletion() {
		deleteTimer?.invalidate()
	}
	
	func updateAmountView() {
		guard let formattedString = tezPadControl.formattedString else { return }
		amountView.text = formattedString
		updateAmountViewFont()
		amountView.centerText()
		setAmountFieldCursorPosition()
	}
	
	func setAmountFieldCursorPosition() {
		guard let text = amountView.text else { return }
		var characterSet = CharacterSet.decimalDigits
		characterSet.insert(charactersIn: tezPadControl.formatter.decimalSeparator)
		if let range = text.rangeOfCharacter(from: characterSet, options: .backwards, range: nil) {
			amountView.selectedTextRange = amountView.textRange(range.upperBound..<range.upperBound)
		}
	}
	
	func updateDecimalPadButtons() {
		decimalButton.isEnabled = !tezPadControl.isEditingFraction
		deleteButton.isEnabled =  tezPadControl.formatter.minimumIntegerDigits > 0 || tezPadControl.formatter.minimumFractionDigits > 0
	}
	
	func updateButtons() {
		payButton.isEnabled = tezPadControl.value > 0
	}
}

final class AmountView: UITextView {
	override func draw(_ rect: CGRect) {
		centerText()
		super.draw(rect)
	}
	override func caretRect(for position: UITextPosition) -> CGRect {
		var rect = super.caretRect(for: position)
		if let font = font {
			rect = rect.insetBy(dx: 0, dy: font.pointSize / 4)
			rect.origin.y += 8 * (font.pointSize / 128)
		}
		return rect
	}
}

extension UITextView {
	func centerText() {
		let offset = max(0, (bounds.height - contentSize.height * zoomScale) / 2.0)
		setContentOffset(CGPoint(x: 0, y: -offset), animated: false)
	}
}
