// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit
import KeychainAccess

protocol TransferRoutingLogic {
	func routeToRequest(with segue: UIStoryboardSegue?)
	func routeToPay(with segue: UIStoryboardSegue?)
}

protocol TransferDataPassingLogic {
	var dataStore: TransferDataStore? { get }
}

final class TransferRouter: TransferRoutingLogic, TransferDataPassingLogic {
	
	var dataStore: TransferDataStore?
	
	weak var viewController: TransferViewController?
	
	func routeToRequest(with segue: UIStoryboardSegue?) {
		guard let segue = segue else { return }
		guard let sourceDataStore = dataStore else { return }
		guard let destination = segue.destination as? RequestViewController else { return }
		guard var destinationDataStore = destination.router?.dataStore else { return }
		passData(from: sourceDataStore, to: &destinationDataStore)
	}
	func routeToPay(with segue: UIStoryboardSegue?) {
		guard let sourceDataStore = dataStore else { return }
		guard let destination = segue?.destination as? OnboardController else { return }
		destination.router = PayRouter()
		guard var destinationDataStore = destination.router?.dataStore as? PayDataStore else { return }
		passData(from: sourceDataStore, to: &destinationDataStore)
	}
	
	func passData(from source: TransferDataStore, to destination: inout RequestDataStore) {
		guard let manager = try? Keychain().getData("public-key").flatMap(Source.init) else { return }
		destination.address = manager.address
	}
	func passData(from source: TransferDataStore, to destination: inout PayDataStore) {
		destination.amount = source.amount
	}
}
