// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation
import Business

typealias TransferValidationLogicCompletion = (Swift.Error?) -> Void

protocol TransferValidationLogic {
	func validate(_ encodedTransfer: BinaryEncodedTransfer, accordingTo transfer: CompiledTransfer, completion: @escaping TransferValidationLogicCompletion)
}

private extension Int {
	var data: Data {
		return Data(
			UInt8(self >> 24 & 0xff),
			UInt8(self >> 16 & 0xff),
			UInt8(self >> 8 & 0xff),
			UInt8(self & 0xff)
		)
	}
}

struct TransferValidator: TransferValidationLogic {
	
	enum Error: Swift.Error {
		case invalidAmount
		case invalidDelegate
		case invalidDestination
		case invalidFees
		case invalidGas
		case invalidParameters
		case invalidScriptCode
		case invalidScriptStorage
		case invalidSourceAddress
		case invalidSourcePublicKey
	}

	func validate(_ encodedTransfer: BinaryEncodedTransfer, accordingTo transfer: CompiledTransfer, completion: @escaping TransferValidationLogicCompletion) {
		switch transfer {
		case let .payment(transfers), let .delegation(transfers):
			validate(encodedTransfer, accordingTo: transfers, completion: completion)
		case let .origination(accounts):
			validate(encodedTransfer, accordingTo: accounts, completion: completion)
		}
	}
	
	private func validate(_ encodedTransfer: BinaryEncodedTransfer, accordingTo transfers: Transfers, completion: @escaping TransferValidationLogicCompletion) {
		let source = transfers.source
		guard let destination = transfers.destinations.first else { return }
		do {
			var parser = PayloadParser(data: encodedTransfer.data)
			var fee: UInt64 = 0
			var gas: UInt64 = 0
			var element = try PayloadElement(from: &parser)
			if case let .reveal(parsedSource, parsedFee, _, parsedGas, _, parsedPublicKey) = element {
				guard String(parsedSource) == source.address else { throw Error.invalidSourceAddress }
				guard String(parsedPublicKey) == source.publicKey else { throw Error.invalidSourcePublicKey }
				fee += parsedFee
				gas += parsedGas
				element = try PayloadElement(from: &parser)
			}
			if case let .transaction(parsedSource, parsedFee, _, parsedGas, _, parsedAmount, parsedDestination, parsedParameters) = element {
				guard String(parsedSource) == source.address else { throw Error.invalidSourceAddress }
				guard parsedAmount == destination.amount else { throw Error.invalidAmount }
				guard String(parsedDestination) == destination.address else { throw Error.invalidDestination }
				fee += parsedFee
				gas += parsedGas
				if let parsedParameters = parsedParameters {
					guard let parameters = destination.parameters else { throw Error.invalidParameters }
					guard parameters.entrypoint == parsedParameters.entrypoint else { throw Error.invalidParameters }
					let sink = DataSink()
					try BinaryEncoder(sink).encode(parameters.expression)
					guard sink.data == parsedParameters.data else { throw Error.invalidParameters }
				} else if destination.parameters != nil {
					throw Error.invalidParameters
				}
			}
			guard fee == encodedTransfer.fees else { throw Error.invalidFees }
			guard gas == encodedTransfer.gas else { throw Error.invalidGas }

			completion(nil)
		} catch {
			completion(error)
		}
	}
	
	private func validate(_ encodedTransfer: BinaryEncodedTransfer, accordingTo accounts: Accounts, completion: @escaping TransferValidationLogicCompletion) {
		let source = accounts.source
		guard let destination = accounts.destinations.first else { return }
		do {
			var parser = PayloadParser(data: encodedTransfer.data)
			var fee: UInt64 = 0
			var gas: UInt64 = 0
			var element = try PayloadElement(from: &parser)
			if case let .reveal(parsedSource, parsedFee, _, parsedGas, _, parsedPublicKey) = element {
				guard String(parsedSource) == source.address else { throw Error.invalidSourceAddress }
				guard String(parsedPublicKey) == source.publicKey else { throw Error.invalidSourcePublicKey }
				fee += parsedFee
				gas += parsedGas
				element = try PayloadElement(from: &parser)
			}
			if case let .origination(parsedSource, parsedFee, _, parsedGas, _, parsedBalance, parsedDelegate, parsedScript) = element {
				guard String(parsedSource) == source.address else { throw Error.invalidSourceAddress }
				guard parsedBalance == destination.balance else { throw Error.invalidAmount }
				guard parsedDelegate.flatMap(String.init) == destination.delegate else { throw Error.invalidDelegate }
				guard parsedScript.code == Script.Manager.data else { throw Error.invalidScriptCode }
				let manager = accounts.source.address
				let expectedStorage = Data(1) + manager.count.data + Data(manager.utf8)
				guard parsedScript.storage == expectedStorage else { throw Error.invalidScriptStorage }
				fee += parsedFee
				gas += parsedGas
			}
			guard fee == encodedTransfer.fees else { throw Error.invalidFees }
			guard gas == encodedTransfer.gas else { throw Error.invalidGas }

			completion(nil)
		} catch {
			completion(error)
		}
	}
}

extension TransferValidationLogic {
	func validate(_ encodedTransfer: BinaryEncodedTransfer, accordingTo transfer: CompiledTransfer) -> AnyExecutable {
		return AnyExecutable { self.validate(encodedTransfer, accordingTo: transfer, completion: $0) }
	}
}

extension TransferValidationLogic {
	func validate(_ encodedTransfer: BinaryEncodedTransfer, accordingTo transfer: CompiledTransfer) -> Work<()> {
		return { promise in
			self.validate(encodedTransfer, accordingTo: transfer) { error in
				promise.fulfill(with: error)
			}
			return AnyDisposable.empty
		}
	}
}
