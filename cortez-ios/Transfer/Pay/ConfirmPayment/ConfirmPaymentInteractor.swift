// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation
import KeychainAccess
import Business

protocol ConfirmPaymentBusinessLogic {
	func execute(_ request: Transfer.Encoding.Request)
	func execute(_ request: Transfer.Injection.Request)
}

protocol ConfirmPaymentDataStore: OnboardPageDataStore {
	var transfer: Transfer? { get set }
}

extension Address {
	
	static let originatedPrefix = "KT1"
	
	var isOriginated: Bool {
		return hasPrefix(.originatedPrefix)
	}
}

final class ConfirmPaymentInteractor: ConfirmPaymentBusinessLogic, ConfirmPaymentDataStore {

	var transfer: Transfer?

	private var compiledTransfer: CompiledTransfer?
	private var encodedTransfer: BinaryEncodedTransfer?
	private var disposable: Disposable?

	let encoder: TransferEncodingLogic
	let validator: TransferValidationLogic
	let injector: TransferInjectionLogic

	let presenter: ConfirmTransferPresentationLogic

	init(encoder: TransferEncodingLogic, validator: TransferValidationLogic, injector: TransferInjectionLogic, presenter: ConfirmTransferPresentationLogic) {
		self.encoder = encoder
		self.validator = validator
		self.injector = injector
		self.presenter = presenter
	}

	func execute(_ request: Transfer.Encoding.Request) {
		guard let transfer = transfer else { return }
		guard let compiledTransfer = transfer.compile() else { return }

		presenter.format(.encoding(transfer))

		self.compiledTransfer = compiledTransfer
		encoder.encode(compiledTransfer) { [weak self] result in
			guard let `self` = self else { return }
			switch result {
			case let .success(encodedTransfer):
				self.encodedTransfer = encodedTransfer
				self.presenter.format(.encoded(fees: encodedTransfer.fees))
			case let .failure(error):
				self.presenter.format(.failed(error: error))
			}
		}
	}

	func execute(_ request: Transfer.Injection.Request) {
		guard let manager = Source.manager else { return }
		guard let transfer = transfer else { return }
		guard let compiledTransfer = compiledTransfer else { return }
		guard let encodedTransfer = encodedTransfer else { return }
		disposable?.dispose()
		disposable = launch(queue: .global(qos: .userInteractive)) {
			do {
				try await(self.validator.validate(encodedTransfer, accordingTo: compiledTransfer))
				let operationHash = try await(self.injector.inject(encodedTransfer.data))
				DispatchQueue.main.async {
					switch transfer {
					case let .delegation(delegation):
						if let store: AnyStore<Account> = Provider.shared.get() {
							var account = delegation.source
							account.delegate = delegation.delegate?.address
							store.update(account)
						}
					case let .origination(origination):
						if let store: AnyStore<Account> = Provider.shared.get() {
							store.insert(
								Account(
									index: -1,
									name: origination.name,
									address: operationHash,
									source: manager.address
								)
							)
						}
					default:
						break
					}
					self.presenter.format(.init(error: nil))
				}
			} catch {
				DispatchQueue.main.async {
					self.presenter.format(.init(error: error))
				}
			}
		}.autodispose()
	}
}
