// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

struct PayloadDecoder {
	func decode(from data: Data) throws -> [PayloadElement] {
		var parser = PayloadParser(data: data)
		var result = [PayloadElement]()
		while !parser.isAtEnd {
			result.append(try PayloadElement(from: &parser))
		}
		return result
	}
}

struct PayloadParser {
	
	enum NumericEncoding {
		case littleEndian
		case bigEndian
		case zarith
	}
	
	private var data: Data
	
	var isAtEnd: Bool {
		return data.isEmpty
	}

	init(data: Data) {
		self.data = data.dropFirst(32) // skip branch
	}
	
	mutating func parse() -> UInt8? {
		return data.isEmpty ? nil : data.removeFirst()
	}
	
	mutating func parse(upTo end: Int) -> Data? {
		let subdata = data.prefix(end)
		guard end == subdata.count else { return nil }
		data = data.dropFirst(end)
		return subdata
	}
	
	mutating func parse(while predicate: (UInt8) -> Bool) -> Data? {
		if data.isEmpty { return nil }
		let subdata = data.prefix(while: predicate)
		data = data.dropFirst(subdata.count)
		return subdata
	}

	mutating func parse<T: FixedWidthInteger>(using encoding: NumericEncoding) -> T? {
		switch encoding {
		case .littleEndian:
			guard let subdata = parse(upTo: MemoryLayout<T>.size) else { return nil }
			return subdata.to(type: T.self)
		case .bigEndian:
			guard let subdata = parse(upTo: MemoryLayout<T>.size) else { return nil }
			return subdata.to(type: T.self)?.bigEndian
		case .zarith:
			guard let reversed = parse(while: { $0 >= 128 })?.reversed() else { return nil }
			let value = reversed.reduce(T.init(parse() ?? 0)) {
				return (T.init($0 << 7) + T.init($1 & 0x7f))
			}
			return value
		}
	}
}

/*
*/
