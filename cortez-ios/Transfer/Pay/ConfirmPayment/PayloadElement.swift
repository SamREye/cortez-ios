// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation
import Sodium
import CryptoSwift

enum OperationTag: UInt8 {
	case endorsement
	case seedNonceRevelation
	case doubleEndorsementEvidence
	case doubleBakingEvidence
	case activateAccount
	case proposals
	case ballot
	case reveal = 107
	case transaction
	case origination
	case delegation
}

enum ContractIDTag: UInt8 {
	case implicit
	case originated
}

enum PublicKeyHashTag: UInt8 {
	case ed25519
	case secp256k1
	case p256
}

enum PublicKeyTag: UInt8 {
	case ed25519
	case secp256k1
	case p256
}

enum EntrypointTag: UInt8 {
	case `default`
	case root
	case `do`
	case setDelegate
	case removeDelegate
	case named = 255
}

enum PayloadElement {

	case reveal(
		source: PublicKeyHash,
		fee: UInt64,
		counter: UInt64,
		gasLimit: UInt64,
		storageLimit: UInt64,
		publicKey: PublicKey
	)
	
	case transaction(
		source: PublicKeyHash,
		fee: UInt64,
		counter: UInt64,
		gasLimit: UInt64,
		storageLimit: UInt64,

		amount: UInt64,
		destination: ContractID,
		parameters: Parameters?
	)
	
	case origination(
		source: PublicKeyHash,
		fee: UInt64,
		counter: UInt64,
		gasLimit: UInt64,
		storageLimit: UInt64,

		balance: UInt64,
		delegate: PublicKeyHash?,
		script: Script
	)

	enum PublicKeyHash {
		case ed25519(Data)
		case secp256k1(Data)
		case p256(Data)
	}

	enum ContractID {
		case implicit(PublicKeyHash)
		case originated(Data)
	}

	enum PublicKey {
		case ed25519(Data)
		case secp256k1(Data)
		case p256(Data)
	}

	struct Script {
		var code: Data
		var storage: Data
	}

	struct Parameters {
		let entrypoint: Entrypoint
		let data: Data
	}
}

extension PayloadElement.PublicKeyHash {
	
	enum Error: Swift.Error {
		case invalidTag
		case invalidSize
	}

	init(from parser: inout PayloadParser) throws {
		guard let tag = parser.parse().flatMap(PublicKeyHashTag.init) else { throw Error.invalidTag }
		guard let data = parser.parse(upTo: 20) else { throw Error.invalidSize }
		switch tag {
		case .ed25519:
			self = .ed25519(data)
		case .secp256k1:
			self = .secp256k1(data)
		case .p256:
			self = .p256(data)
		}
	}
}

extension PayloadElement.ContractID {
	
	enum Error: Swift.Error {
		case invalidTag
		case invalidSize
	}
	
	init(from parser: inout PayloadParser) throws {
		guard let tag = parser.parse().flatMap(ContractIDTag.init) else { throw Error.invalidTag }
		switch tag {
		case .implicit:
			self = .implicit(try PayloadElement.PublicKeyHash(from: &parser))
		case .originated:
			guard let hash = parser.parse(upTo: 21) else { throw Error.invalidSize }
			self = .originated(hash.dropLast())
		}
	}
}

extension PayloadElement.ContractID: CustomStringConvertible {
	var description: String {
		return String(self) ?? "\(self)"
	}
}

extension PayloadElement.PublicKey: CustomStringConvertible {
	var description: String {
		return String(self) ?? "\(self)"
	}
}

struct Prefix {

	let data: Data

	static let edsk = Prefix(data: Data([UInt8](arrayLiteral: 43, 246, 78, 7)))
	static let edpk = Prefix(data: Data([UInt8](arrayLiteral: 13, 15, 37, 217)))
	
	static let spsk = Prefix(data: Data([UInt8](arrayLiteral: 17, 162, 224, 201)))
	static let sppk = Prefix(data: Data([UInt8](arrayLiteral: 3, 254, 226, 86)))
	
	static let p2sk = Prefix(data: Data([UInt8](arrayLiteral: 16, 81, 238, 189)))
	static let p2pk = Prefix(data: Data([UInt8](arrayLiteral: 3, 178, 139, 127)))

	static let tz1 = Prefix(data: Data([UInt8](arrayLiteral: 6, 161, 159)))
	static let tz2 = Prefix(data: Data([UInt8](arrayLiteral: 6, 161, 161)))
	static let tz3 = Prefix(data: Data([UInt8](arrayLiteral: 6, 161, 164)))

	static let kt1 = Prefix(data: Data([UInt8](arrayLiteral: 2, 90, 121)))
}

extension Data {
	var checksum: Data {
		return sha256().sha256().prefix(4)
	}
	
	func encode(with prefix: Prefix) -> String {
		let prefixedData = prefix.data + self
		return String(base58Encoding: Data(prefixedData + prefixedData.checksum))
	}
}

extension String {
	init?(_ contractID: PayloadElement.ContractID) {
		switch contractID {
		case let .implicit(.ed25519(data)): self = data.encode(with: .tz1)
		case let .implicit(.secp256k1(data)): self = data.encode(with: .tz2)
		case let .implicit(.p256(data)): self = data.encode(with: .tz3)
		case let .originated(data): self = data.encode(with: .kt1)
		}
	}
	
	init?(_ publicKey: PayloadElement.PublicKey) {
		switch publicKey {
		case let .ed25519(data): self = data.encode(with: .edpk)
		case let .secp256k1(data): self = data.encode(with: .edpk)
		case let .p256(data): self = data.encode(with: .p2pk)
		}
	}

	init?(_ publicKeyHash: PayloadElement.PublicKeyHash) {
		switch publicKeyHash {
		case let .ed25519(data): self = data.encode(with: .tz1)
		case let .secp256k1(data): self = data.encode(with: .tz2)
		case let .p256(data): self = data.encode(with: .tz3)
		}
	}
}

extension PayloadElement.PublicKey {
	
	enum Error: Swift.Error {
		case invalidTag
		case invalidSize
	}
	
	init(from parser: inout PayloadParser) throws {
		guard let tag = parser.parse().flatMap(PublicKeyTag.init) else { throw Error.invalidTag }
		switch tag {
		case .ed25519:
			guard let key = parser.parse(upTo: 32) else { throw Error.invalidSize }
			self = .ed25519(key)
		case .secp256k1:
			guard let key = parser.parse(upTo: 33) else { throw Error.invalidSize }
			self = .secp256k1(key)
		case .p256:
			guard let key = parser.parse(upTo: 33) else { throw Error.invalidSize }
			self = .p256(key)
		}
	}
}

extension PayloadElement.Parameters {
	
	enum Error: Swift.Error {
		case invalidSize
	}

	init(form parser: inout PayloadParser) throws {
		entrypoint = try Entrypoint(form: &parser)
		guard let size: Int32 = parser.parse(using: .bigEndian) else { throw Error.invalidSize }
		guard let data = parser.parse(upTo: Int(size)) else { throw Error.invalidSize }
		self.data = data
	}
}

extension Entrypoint {

	enum Error: Swift.Error {
		case invalidTag
		case invalidSize
	}
	
	init(form parser: inout PayloadParser) throws {
		guard let tag = parser.parse().flatMap(EntrypointTag.init) else { throw Error.invalidTag }
		switch tag {
		case .default: self = .default
		case .root: self = .root
		case .do: self = .do
		case .setDelegate: self = .setDelegate
		case .removeDelegate: self = .removeDelegate
		case .named:
			guard let size = parser.parse() else { throw Error.invalidSize }
			guard let data = parser.parse(upTo: Int(size)) else { throw Error.invalidSize }
			guard let name = String(data: data, encoding: .utf8) else { throw Error.invalidSize }
			self = .named(name)
		}
	}
}

extension PayloadElement {
	
	enum Error: Swift.Error {
		case invalidTag
		case invalidSize

		// reveal
		case invalidRevealSource(reason: Swift.Error)
		case invalidRevealFee
		case invalidRevealCounter
		case invalidRevealGasLimit
		case invalidRevealStorageLimit
		case invalidRevealPublicKey(reason: Swift.Error)
		
		// transaction
		case invalidTransactionSource(reason: Swift.Error)
		case invalidTransactionFee
		case invalidTransactionCounter
		case invalidTransactionGasLimit
		case invalidTransactionStorageLimit
		case invalidTransactionAmount
		case invalidTransactionDestination(reason: Swift.Error)
		case invalidTransactionParameters(reason: Swift.Error)
		
		// origination
		case invalidOriginationSource(reason: Swift.Error)
		case invalidOriginationFee
		case invalidOriginationCounter
		case invalidOriginationGasLimit
		case invalidOriginationStorageLimit
		case invalidOriginationManager(reason: Swift.Error)
		case invalidOriginationBalance
		case invalidOriginationSpendable
		case invalidOriginationDelegatable
		case invalidOriginationDelegate(reason: Swift.Error)
		case invalidOriginationScript

		case unimplemented
	}
	
	init(from parser: inout PayloadParser) throws {
		guard let tag = parser.parse().flatMap(OperationTag.init) else { throw Error.invalidTag }
		switch tag {
		case .reveal: self = try .reveal(from: &parser)
		case .transaction: self = try .transaction(from: &parser)
		case .origination: self = try .origination(from: &parser)
		default: throw Error.unimplemented
		}
	}
	
	static func reveal(from parser: inout PayloadParser) throws -> PayloadElement {
		return .reveal(
			source: try publicKeyHash(from: &parser) { .invalidRevealSource(reason: $0) },
			fee: try zarithNumber(from: &parser, error: .invalidRevealFee),
			counter: try zarithNumber(from: &parser, error: .invalidRevealCounter),
			gasLimit: try zarithNumber(from: &parser, error: .invalidRevealGasLimit),
			storageLimit: try zarithNumber(from: &parser, error: .invalidRevealStorageLimit),
			publicKey: try PublicKey(from: &parser)
		)
	}

	static func transaction(from parser: inout PayloadParser) throws -> PayloadElement {
		return .transaction(
			source: try publicKeyHash(from: &parser) { .invalidTransactionSource(reason: $0) },
			fee: try zarithNumber(from: &parser, error: .invalidTransactionFee),
			counter: try zarithNumber(from: &parser, error: .invalidTransactionCounter),
			gasLimit: try zarithNumber(from: &parser, error: .invalidTransactionGasLimit),
			storageLimit: try zarithNumber(from: &parser, error: .invalidTransactionStorageLimit),
			amount: try zarithNumber(from: &parser, error: .invalidTransactionAmount),
			destination: try contractID(from: &parser) { .invalidTransactionDestination(reason: $0) },
			parameters: try parameters(from: &parser) { .invalidTransactionParameters(reason: $0) }
		)
	}
	
	static func origination(from parser: inout PayloadParser) throws -> PayloadElement {
		return .origination(
			source: try publicKeyHash(from: &parser) { .invalidOriginationSource(reason: $0) },
			fee: try zarithNumber(from: &parser, error: .invalidOriginationFee),
			counter: try zarithNumber(from: &parser, error: .invalidOriginationCounter),
			gasLimit: try zarithNumber(from: &parser, error: .invalidOriginationGasLimit),
			storageLimit: try zarithNumber(from: &parser, error: .invalidOriginationStorageLimit),
			balance: try zarithNumber(from: &parser, error: .invalidOriginationBalance),
			delegate: try delegateIfPresent(from: &parser) { .invalidOriginationDelegate(reason: $0) },
			script: try script(from: &parser, error: .invalidOriginationScript)
		)
	}
	
	private static func contractID(from parser: inout PayloadParser, error: (Swift.Error) -> Error) throws -> ContractID {
		do {
			return try ContractID(from: &parser)
		} catch let reason {
			throw error(reason)
		}
	}
	
	private static func zarithNumber<T: FixedWidthInteger>(from parser: inout PayloadParser, error: @autoclosure () -> Error) throws -> T {
		guard let value: T = parser.parse(using: .zarith) else { throw error() }
		return value
	}

	private static func parameters(from parser: inout PayloadParser, error: (Swift.Error) -> Error) throws -> Parameters? {
		do {
			let hasParameters = try bool(from: &parser)
			guard hasParameters else { return nil }
			return try Parameters(form: &parser)
		} catch let reason {
			throw error(reason)
		}
	}

	private static func publicKeyHash(from parser: inout PayloadParser, error: (Swift.Error) -> Error) throws -> PublicKeyHash {
		do {
			return try PublicKeyHash(from: &parser)
		} catch let reason {
			throw error(reason)
		}
	}

	private static func bool(from parser: inout PayloadParser) throws -> Bool {
		guard let value = parser.parse().map({ $0 == 255 }) else { throw Error.invalidSize }
		return value
	}
	
	private static func int(from parser: inout PayloadParser, error: @autoclosure () -> Error) throws -> Int {
		guard let data = parser.parse(upTo: 4) else { throw error() }
		return data.reduce(0) { $0 << 8 | Int($1) }
	}
	
	private static func delegateIfPresent(from parser: inout PayloadParser, error: (Swift.Error) -> Error) throws -> PublicKeyHash? {
		do {
			let hasDelegate = try bool(from: &parser)
			guard hasDelegate else { return nil }
			return try publicKeyHash(from: &parser, error: error)
		} catch let reason {
			throw error(reason)
		}
	}
	
	private static func script(from parser: inout PayloadParser, error: @autoclosure () -> Error) throws -> Script {
		guard let code = parser.parse(upTo: try int(from: &parser, error: error())) else { throw error() }
		guard let storage = parser.parse(upTo: try int(from: &parser, error: error())) else { throw error() }
		return Script(code: code, storage: storage)
	}
}
