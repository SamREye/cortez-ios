// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ListContactsDataStore: OnboardPageDataStore {
	var selectedContact: Contact? { get set }
}

protocol ListContactsBusinessLogic {
	func selectContact(at indexPath: IndexPath)
	func execute(_ request: Contact.Search.Request)
}

final class ListContactsInteractor: ListContactsBusinessLogic, ListContactsDataStore {

	var selectedContact: Contact?

	var selection: AnySelection<Contact>?

	let presenter: ListContactsPresentationLogic
	
	init(presenter: ListContactsPresentationLogic) {
		self.presenter = presenter
		
		reload()
	}
	
	func selectContact(at indexPath: IndexPath) {
		selectedContact = selection?[indexPath]
	}
	
	func execute(_ request: Contact.Search.Request) {
		if request.query.isEmpty {
			reload()
			return
		}
		guard let store: AnyStore<Contact> = Provider.shared.get() else { return }
		let selection = store.select(
			Query.where(.nameContains(request.query))
				.sorted(by: .indexTitle, ascending: true)
				.sorted(by: .name, ascending: true)
		)
		self.selection = selection
		presenter.format(.init(selection: selection))
	}
	
	func reload() {
		guard let store: AnyStore<Contact> = Provider.shared.get() else { return }
		let selection = store.select(
			Query.where(.all)
				.sorted(by: .indexTitle, ascending: true)
				.sorted(by: .name, ascending: true)
		)
		self.selection = selection
		presenter.format(.init(selection: selection))
	}
}
