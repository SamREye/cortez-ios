// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

final class ListContactsRouter: OnboardPageRoutingLogic, OnboardPageDataPassingLogic {
	
	var dataStore: OnboardPageDataStore?

	weak var viewController: ListContactsViewController?
	
	var contactViewController: UIViewController? {
		return viewController.map { _ in
			UIStoryboard(name: "Transfer", bundle: nil).instantiateViewController(withIdentifier: "confirm-payment")
		}
	}
	
	func routeToNextPage(with segue: UIStoryboardSegue?) {
		guard let source = viewController else { return }
		if let onboardController = source.onboardController {
			onboardController.router?.routeToNextPage(with: nil)
			return
		}
		guard let sourceDataStore = dataStore as? ListContactsDataStore else { return }
		guard let parent = source.presentingViewController as? OnboardController else { return }
		guard let destination = parent.pageViewController.viewControllers?.first as? DelegateAccountViewController else { return }
		guard var destinationDataStore = destination.router?.dataStore as? DelegateAccountDataStore else { return }
		passData(from: sourceDataStore, to: &destinationDataStore)
		navigate(from: source, to: destination)
	}

	func passData(from source: OnboardPageDataStore, to destination: inout OnboardDataStore) {
		guard let source = source as? ListContactsDataStore else { return }
		guard var destination = destination as? PayDataStore else { return }
		destination.destination = source.selectedContact
	}
	func passData(from source: ListContactsDataStore, to destination: inout DelegateAccountDataStore) {
		destination.selectedDelegate = source.selectedContact
		destination.isDelegationEnabled = source.selectedContact != nil
	}
	
	func navigate(from source: ListContactsViewController, to destination: DelegateAccountViewController) {
		source.dismiss(animated: true, completion: nil)
	}
}
