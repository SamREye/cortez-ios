// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit
import ContactsUI

class CreateContactViewController: UIViewController {

	var router: (CreateContactRoutingLogic & CreateContactDataPassingLogic)?
	var interactor: CreateContactBusinessLogic?

	@IBOutlet weak var nameField: ValidableTextField!
	@IBOutlet weak var nameBar: UIView!
	@IBOutlet weak var addressField: ValidableTextField!
	@IBOutlet weak var addressBar: UIView!

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	private func setup() {
		let router = CreateContactRouter()
		let interactor = CreateContactInteractor()
		router.dataStore = interactor
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		nameField.text = router?.dataStore?.name
		addressField.text = router?.dataStore?.address

		nameField.inputAccessoryView = nameBar
		addressField.inputAccessoryView = addressBar
		addressField.validator = {
			$0.count >= 4
		}
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		DispatchQueue.main.async {
			if self.nameField.text == nil || self.nameField.text == .empty {
				self.nameField.becomeFirstResponder()
			} else {
				self.addressField.becomeFirstResponder()
			}
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		nameField.resignFirstResponder()
		addressField.resignFirstResponder()
	}
	
	@IBAction func importFromMyContacts() {
		let picker = CNContactPickerViewController()
		picker.delegate = self
		show(picker, sender: nil)
	}
	
	@IBAction func create() {
		// TODO: check name & address
		guard let name = nameField.text, let address = addressField.text else { return }
		guard let store: ContactStoringLogic = Provider.shared.get() else { return }
		store.insert(Contact(name: name, address: address)) {
			switch $0 {
			case .success:
				self.performSegue(withIdentifier: "unwind", sender: self)
			case .failure(let error):
				debug(error)
			}
		}
	}
	
	@IBAction func askForAddress() {
		addressField.becomeFirstResponder()
	}
	
	@IBAction func setReturnKeyType(_ textField: ValidableTextField) {
		let otherTextField: ValidableTextField = textField == nameField ? addressField : nameField
		textField.returnKeyType = otherTextField.hasText ? .done : .next
	}
	
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
		if let destination = segue.destination as? QRCodeScannerViewController {
			destination.view.bringSubviewToFront(destination.view.subviews.first!)
			destination.delegate = self
		}
    }
	
	@IBAction func unwindToCreateContact(segue: UIStoryboardSegue) {
		// nothing to do
	}
}

extension CreateContactViewController: CNContactPickerDelegate {
	func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
		nameField.text = contact.name
	}
}

extension CreateContactViewController: QRCodeScannerViewControllerDelegate {
	func scannerViewController(_ scannerViewController: QRCodeScannerViewController, didScan stringValue: String) {
		addressField.text = stringValue
		addressField.becomeFirstResponder()
	}
}

extension CreateContactViewController {
	class func instantiateFromStoryboard() -> CreateContactViewController? {
		let storyboard = UIStoryboard(name: "ListContacts", bundle: nil)
		return storyboard.instantiateViewController(withIdentifier: "create-contact") as? CreateContactViewController
	}
}
