// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

extension BinaryFloatingPoint {
	var mutez: Self {
		return self * 1000000
	}
	var tez: Self {
		return self / 1000000
	}
}

extension BinaryInteger {
	var mutez: Self {
		return self * 1000000
	}
	var tez: Self {
		return self / 1000000
	}
}

extension NumberFormatter {
	static func tez() -> NumberFormatter {
		let formatter = NumberFormatter()
		formatter.numberStyle = .currency
		formatter.locale = .current
		formatter.currencySymbol = "A"
		formatter.maximumIntegerDigits = 9
		formatter.minimumFractionDigits = 0
		formatter.maximumFractionDigits = 6
		return formatter
	}
}

@objc protocol TezPadControlDelegate: class {
	func tezPadControlDidUpdateAmount(_ tezPadControl: TezPadControl)
}

class TezPadControl: NSObject {

	var value: UInt64 {
		return UInt64(integer.mutez + fraction * fractionPaddingCounter)
	}
	var formattedString: String? {
		let minimumIntegerDigits = formatter.minimumIntegerDigits
		defer { formatter.minimumIntegerDigits = minimumIntegerDigits }
		if minimumIntegerDigits == 0 {
			formatter.minimumIntegerDigits = 1
		}
		return formatter.string(for: Double(value).tez)
	}
	var isEditingFraction = false {
		didSet {
			formatter.alwaysShowsDecimalSeparator = isEditingFraction
		}
	}
	
	@IBOutlet weak var delegate: TezPadControlDelegate?

	let formatter: NumberFormatter = {
		let formatter: NumberFormatter = .tez()
		formatter.minimumIntegerDigits = 0
		formatter.minimumFractionDigits = 0
		return formatter
	}()

	private var integer: UInt = 0
	private var fraction: UInt = 0
	private var fractionPaddingCounter: UInt = UInt(1).mutez

	func updateAmount(with string: String) {
		defer { delegate?.tezPadControlDidUpdateAmount(self) }
		if string.isEmpty { // delete
			if formatter.minimumFractionDigits > 0 {
				fractionPaddingCounter *= 10
				formatter.minimumFractionDigits -= 1
				fraction /= 10
			} else if formatter.minimumIntegerDigits > 0 {
				if isEditingFraction {
					isEditingFraction = false
				} else {
					integer /= 10
					formatter.minimumIntegerDigits -= 1
				}
			}
			return
		}
		
		if string == formatter.decimalSeparator {
			isEditingFraction = true
			if formatter.minimumIntegerDigits == 0 {
				formatter.minimumIntegerDigits = 1
			}
			return
		}
		guard let digit = UInt(string), digit < 10 else { return }
		if isEditingFraction {
			if formatter.minimumFractionDigits < formatter.maximumFractionDigits {
				fraction *= 10
				fraction += digit
				fractionPaddingCounter /= 10
				formatter.minimumFractionDigits += 1
			}
		} else if formatter.minimumIntegerDigits < formatter.maximumIntegerDigits {
			integer *= 10
			integer += digit
			formatter.minimumIntegerDigits += 1
		} else if formatter.minimumFractionDigits < formatter.maximumFractionDigits {
			fraction *= 10
			fraction += digit
			fractionPaddingCounter /= 10
			isEditingFraction = true
			formatter.minimumFractionDigits += 1
		}
	}
	
	@IBAction func clear() {
		integer = 0
		fraction = 0
		fractionPaddingCounter = 1000000
		isEditingFraction = false
		formatter.minimumIntegerDigits = 0
		formatter.minimumFractionDigits = 0
		delegate?.tezPadControlDidUpdateAmount(self)
	}
}

extension TezPadControl: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		updateAmount(with: string)
		return false
	}
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		return true
	}
}

extension TezPadControl: UITextViewDelegate {
	func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
		updateAmount(with: text)
		return false
	}
}
