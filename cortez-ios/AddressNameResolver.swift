// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

final class AddressNameResolver {
	
	var operations: AnySelection<Operation>?
	var accounts: AnySelection<Account>?

	let contacts: AnySelection<Contact>?
	
	var index: [Address: Set<String>] = [:]
	
	struct OperationObserver: SelectionDelegate {
		func selection<T: Selection>(_ selection: T, didInsert operation: Operation, at indexPath: IndexPath) where Operation == T.SectionElement {
			guard let store: AnyStore<Contact> = Provider.shared.get() else { return }
			store.enqueue { context in
				guard let store: AnyStore<Operation> = Provider.shared.get() else { return }
				let query = Query<Contact>.where(.addressIn([operation.source, operation.destination])).sorted(by: .name, ascending: true)
				let contacts = try context.select(query)
				let operation = contacts.reduce(nil as Operation?) {
					guard let operation = ($0 ?? operation).resolvingAddressNames(with: $1) else {
						return $0
					}
					return operation
				}
				guard let resolvedOperation = operation else { return }
				store.update(resolvedOperation)
			}
		}
	}

	struct AccountObserver: SelectionDelegate {
		func selection<T: Selection>(_ selection: T, didInsert account: Account, at indexPath: IndexPath) where Account == T.SectionElement {
			guard let store: AnyStore<Contact> = Provider.shared.get() else { return }
			var account = account
			store.enqueue { context in
				guard let store: AnyStore<Account> = Provider.shared.get() else { return }
				let query = Query<Contact>.where(.addressEquals(account.address)).sorted(by: .name, ascending: true)
				guard let contact = try context.select(query).first else { return }
				guard account.resolveAddressName(with: contact) else { return }
				store.update(account)
			}
		}
	}
	
	struct ContactObserver: SelectionDelegate {
		func selection<T: Selection>(_ selection: T, didInsert contact: Contact, at indexPath: IndexPath) where Contact == T.SectionElement {
			updateOperations(with: contact)
			updateAccounts(with: contact)
		}
		func selection<T: Selection>(_ selection: T, didUpdate contact: Contact, at indexPath: IndexPath) where Contact == T.SectionElement {
			updateOperations(with: contact)
			updateAccounts(with: contact)
		}
		
		private func updateOperations(with contact: Contact) {
			guard let store: AnyStore<Operation> = Provider.shared.get() else { return }
			store.enqueue { context in
				let query = Query<Operation>.where(.sourceOrDestinationEquals(contact.address)).sorted(by: .operationHash, ascending: false)
				let operations = try context.select(query)
				try operations.reduce(into: []) {
					guard let operation = $1.resolvingAddressNames(with: contact) else { return }
					$0.append(operation)
				}.forEach(context.update)
			}
		}
		
		private func updateAccounts(with contact: Contact) {
			guard let store: AnyStore<Account> = Provider.shared.get() else { return }
			store.enqueue { context in
				let query = Query<Account>.where(.addressEquals(contact.address)).sorted(by: .index, ascending: false)
				let operations = try context.select(query)
				try operations.reduce(into: []) {
					guard let account = $1.resolvingAddressName(with: contact) else { return }
					$0.append(account)
				}.forEach(context.update)
			}
		}
	}
	
	init() {
		contacts = {
			guard let store: AnyStore<Contact> = Provider.shared.get() else { return nil }
			let query = Query<Contact>.where(.all).sorted(by: .indexTitle, ascending: true).sorted(by: .name, ascending: true)
			var selection = store.select(query)
			selection.delegate = AnySelectionDelegate(ContactObserver())
			return selection
		}()
		Cortez.userDidLogin.addObserver(self, selector: #selector(reload))
		// not mandatory for contacts
		
		reload()
	}
	deinit {
		Cortez.userDidLogin.removeObserver(self)
	}
	
	private func resolve() {
		guard let store: AnyStore<Contact> = Provider.shared.get() else { return }
		let query = Query<Contact>.where(.all).sorted(by: .indexTitle, ascending: true).sorted(by: .name, ascending: true)
		store.enqueue { [weak self] in self?.updateOperations(with: try $0.select(query)) }
		store.enqueue { [weak self] in self?.updateAccounts(with: try $0.select(query)) }
	}
	
	private func updateOperations<T: Sequence>(with contacts: T) where Contact == T.Element {
		guard let store: AnyStore<Operation> = Provider.shared.get() else { return }
		store.enqueue { context in
			let index: [Address: String] = contacts.reduce(into: [:]) { $0[$1.address] = $1.name }
			let query = Query<Operation>.where(.sourceOrDestinationIn(Array(index.keys))).sorted(by: .operationHash, ascending: false)
			let operations = try context.select(query)
			try operations.reduce(into: []) {
				guard let operation = $1.resolvingAddressNames(with: index) else { return }
				$0.append(operation)
			}.forEach(context.update)
		}
	}
	private func updateAccounts<T: Sequence>(with contacts: T) where Contact == T.Element {
		guard let store: AnyStore<Account> = Provider.shared.get() else { return }
		store.enqueue { context in
			let index: [Address: String] = contacts.reduce(into: [:]) { $0[$1.address] = $1.name }
			let query = Query<Account>.where(.addressIn(Array(index.keys))).sorted(by: .index, ascending: true)
			let accounts = try context.select(query)
			try accounts.reduce(into: []) {
				guard let account = $1.resolvingAddressName(with: index) else { return }
				$0.append(account)
			}.forEach(context.update)
		}
	}
	
	@objc private func reload() {
		operations = {
			guard let store: AnyStore<Operation> = Provider.shared.get() else { return nil }
			let query = Query<Operation>.where(.all).sorted(by: .indexTitle, ascending: true).sorted(by: .timestamp, ascending: true)
			var selection = store.select(query)
			selection.delegate = AnySelectionDelegate(OperationObserver())
			return selection
		}()
		accounts = {
			guard let store: AnyStore<Account> = Provider.shared.get() else { return nil }
			var selection = store.select(Query.where(.all).sorted(by: .index, ascending: true))
			selection.delegate = AnySelectionDelegate(AccountObserver())
			return selection
		}()
		resolve()
	}
}

private extension Operation {
	mutating func resolveAddressNames(with contact: Contact) -> Bool {
		var hasChanges = false
		if contact.address == source && contact.name != sourceName {
			sourceName = contact.name
			hasChanges = true
		}
		if contact.address == destination && contact.name != destinationName {
			destinationName = contact.name
			hasChanges = true
		}
		return hasChanges
	}
	func resolvingAddressNames(with contact: Contact) -> Operation? {
		var operation = self
		return operation.resolveAddressNames(with: contact) ? operation : nil
	}
	
	mutating func resolveAddressNames(with index: [Address: String]) -> Bool {
		let newSourceName = index[source]
		let newDestinationName = index[destination]
		guard sourceName != newSourceName || destinationName != newDestinationName else { return false }
		sourceName = newSourceName
		destinationName = newDestinationName
		return true
	}
	func resolvingAddressNames(with index: [Address: String]) -> Operation? {
		var operation = self
		return operation.resolveAddressNames(with: index) ? operation : nil
	}
}

private extension Account {
	mutating func resolveAddressName(with contact: Contact) -> Bool {
		guard address == contact.address else { return false }
		name = contact.name
		return true
	}
	func resolvingAddressName(with contact: Contact) -> Account? {
		var account = self
		return account.resolveAddressName(with: contact) ? account : nil
	}

	mutating func resolveAddressName(with index: [Address: String]) -> Bool {
		guard let newName = index[address] else { return false }
		name = newName
		return true
	}
	func resolvingAddressName(with index: [Address: String]) -> Account? {
		var account = self
		return account.resolveAddressName(with: index) ? account : nil
	}
}
