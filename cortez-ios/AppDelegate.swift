// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

import MnemonicKit
import Sodium
import KeychainAccess
import LocalAuthentication

extension String {
	static let versionKey = "version"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	
	var launchScreen: UIWindow?

	var addressNameResolver: AddressNameResolver?

	private var publicKeyExists: Bool {
		return (try? Keychain().contains("public-key")) ?? false
	}

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
	
		let navigationBarAppearance = UINavigationBar.appearance()
		navigationBarAppearance.shadowImage = UIImage()

		let toolbarAppearance = UIToolbar.appearance()
		toolbarAppearance.setShadowImage(UIImage(), forToolbarPosition: .any)
		
		let provider = Provider.shared
		let configuration: URLSessionConfiguration = .default
		configuration.timeoutIntervalForRequest = 10
		//#if DEBUG
		//provider.set(URLSession(configuration: configuration, delegate: self, delegateQueue: .main))
		//#else
		provider.set(URLSession(configuration: configuration, delegate: nil, delegateQueue: .main))
		//#endif
		do {
			provider.set(try AnyStore<Contact>(CoreDataStore("Contacts")))
			provider.set(try AnyStore<Operation>(CoreDataStore("Operations")))
			provider.set(try AnyStore<Account>(CoreDataStore("Accounts")))
			// TODO: create main account if needed
			// TODO: rename myself to Main Account in Contacts and Operations
		} catch {
			debug(error)
		}
		
		migrate()

		addressNameResolver = AddressNameResolver()
		
		if publicKeyExists {
			Cortez.userDidLogin.post()
		}

		askForCredentialsIfNeeded()

		return true
	}
	
	private func migrate() {
		guard let tz1 = Source.manager?.address else { return }
		guard let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else { return }
		debug("version: \(currentVersion)")
		if let previousVersion = UserDefaults.standard.string(forKey: .versionKey) {
			if currentVersion == previousVersion { return }
			debug("migrate from \(previousVersion) to \(currentVersion)")
		} else {
			debug("migrate")
		}
		guard let store: AnyStore<Contact> = Provider.shared.get() else { return }
		store.enqueue {
			guard let contact = try? $0.select(Query.where(.addressEquals(tz1)).sorted(by: .name, ascending: true)).first else { return }
			// if old name change it
			if contact.name == "myself" {
				// delete any contact named "Main Account" to avoid conflicts
				if let contacts = try? $0.select(Query.where(.nameEquals(.defaultMainAccountName)).sorted(by: .name, ascending: true)) {
					try? contacts.forEach($0.delete)
				}
				do {
					try $0.update(Contact(name: .defaultMainAccountName, address: tz1))
					DispatchQueue.main.async {
						UserDefaults.standard.setValue(currentVersion, forKey: .versionKey)
					}
				} catch {
					debug(error)
				}
			} else {
				DispatchQueue.main.async {
					UserDefaults.standard.setValue(currentVersion, forKey: .versionKey)
				}
			}
		}
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
		askForCredentialsIfNeeded()
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
	
	func askForCredentialsIfNeeded() {
		guard Settings.shared.isCredentialsNeededWhenAppBecomesActive else { return }
		guard launchScreen == nil else { return }
		askForCredentials()
	}
	func askForCredentials() {
		
		showLaunchScreenIfNeeded()

		let context = LAContext()
		var error: NSError?
		if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
			context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "Please authenticate yourself to continue") { success, error in
				guard success else {
					DispatchQueue.main.async(execute: self.askForCredentials)
					return
				}
				DispatchQueue.main.async(execute: self.hideLaunchScreen)
			}
		} else if let error = error {
			let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
			let okButton = UIAlertAction(title: "Ok", style: .default) { [weak self] _ in
				self?.askForCredentials()
			}
			alert.addAction(okButton)
			launchScreen?.rootViewController?.present(alert, animated: true, completion: nil)
		} else {
			hideLaunchScreen()
		}
	}
	
	func showLaunchScreenIfNeeded() {
		guard launchScreen == nil else { return }
		showLaunchScreen()
	}
	func showLaunchScreen() {
		let storyboard = UIStoryboard(name: "LaunchScreen", bundle: nil)
		guard let viewController = storyboard.instantiateInitialViewController() else { return }
		let window = UIWindow(frame: UIScreen.main.bounds)
		window.rootViewController = viewController
		window.windowLevel = .alert + 1
		window.makeKeyAndVisible()
		launchScreen = window
	}
	func hideLaunchScreen() {
		//launchScreen?.rootViewController?.dismiss(animated: true) {
		//	self.window?.makeKeyAndVisible()
		//	self.launchScreen = nil
		//}
		window?.makeKeyAndVisible()
		launchScreen = nil
	}
}

#if DEBUG
extension AppDelegate: URLSessionDelegate {
	func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
		completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
	}
}
#endif
