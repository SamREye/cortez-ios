// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

struct Resource {
	let request: URLRequest
	let reducer: (Response) -> Void
}

extension Resource {
	init(remoteProcedure: RemoteProcedure, reducer: @escaping (Response) -> Void) {
		self.init(request: remoteProcedure.request, reducer: reducer)
	}
}

final class Downloader {

	typealias Callback = (Call.Error?) -> Void

	let session: URLSession

	final class Download {

		let call: Call

		var callbacks = [UUID: Callback]()

		init(call: Call) {
			self.call = call
		}
	}

	private var downloads = [URLRequest: Download]()

	init(session: URLSession = .shared) {
		self.session = session
	}

	func download(resource: Resource, callback: @escaping Callback) -> Cancellable {
		let key = UUID()
		download(for: resource).callbacks[key] = callback
		return AnyCancellable { [weak self] in self?.remove(resource.request, callbackForKey: key) }
	}
	func cancelDownload(for resource: Resource) {
		guard let download = downloads[resource.request] else { return }
		download.call.cancel()
	}
	
	private func download(for resource: Resource) -> Download {
		let request = resource.request
		if let download = downloads[request] {
			return download
		}
		let call = Call(request: request, session: session)
		call.enqueue { [weak self] in
			switch $0 {
			case let .failure(error): self?.dispatch(error, for: request)
			case let .success(value): resource.reducer(value)
			}
		}
		let download = Download(call: call)
		downloads[request] = download
		return download
	}

	private func dispatch(_ error: Call.Error, for request: URLRequest) {
		guard let download = downloads.removeValue(forKey: request) else { return }
		download.callbacks.values.forEach { $0(error) }
	}

	private func remove(_ request: URLRequest, callbackForKey key: UUID) {
		guard let download = self.downloads[request] else { return }
		download.callbacks[key] = nil
		if download.callbacks.isEmpty {
			self.downloads[request] = nil
		}
	}
}
