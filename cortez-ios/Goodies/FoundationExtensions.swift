// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

extension DispatchQueue {
	func add(group: DispatchGroup? = nil, qos: DispatchQoS = .unspecified, flags: DispatchWorkItemFlags = [],
			   execute work: @escaping @convention(block) () -> Void) -> DispatchWorkItem {
		let workItem = DispatchWorkItem(qos: qos, flags: flags, block: work)
		if let group = group {
			async(group: group, execute: workItem)
		} else {
			async(execute: workItem)
		}
		return workItem
	}
}

extension URL {
	func appendingPathComponent<T: RawRepresentable>(_ pathComponent: T) -> URL where String == T.RawValue {
		return appendingPathComponent(pathComponent.rawValue)
	}

	func appendingPathComponents(_ pathComponents: [String]) -> URL {
		return pathComponents.reduce(self) { $0.appendingPathComponent($1) }
	}
	func appendingPathComponents(_ pathComponents: String...) -> URL {
		return appendingPathComponents(pathComponents)
	}

	func appendingQueryItems(_ queryItems: [URLQueryItem]) -> URL {
		guard var components = URLComponents(url: self, resolvingAgainstBaseURL: true) else { return self }
		components.queryItems = queryItems
		return components.url ?? self
	}

	func appendingQueryItems(_ queryItems: URLQueryItem...) -> URL {
		return appendingQueryItems(queryItems)
	}
}

extension CharacterSet {
	func updating(with scalar: Unicode.Scalar) -> CharacterSet {
		var characterSet = self
		characterSet.update(with: scalar)
		return characterSet
	}
}

extension JSONDecoder {
	convenience init(dateDecodingStrategy: DateDecodingStrategy = .deferredToDate,
					 keyDecodingStrategy: KeyDecodingStrategy = .useDefaultKeys) {
		self.init()
		self.dateDecodingStrategy = dateDecodingStrategy
		self.keyDecodingStrategy = keyDecodingStrategy
	}
}

extension FileManager {
	var documentURL: URL? {
		return urls(for: .documentDirectory, in: .userDomainMask).first
	}
}

extension Notification.Name {
	func addObserver(_ observer: Any, selector: Selector, object: Any? = nil, to notificationCenter: NotificationCenter = .default) {
		notificationCenter.addObserver(observer, selector: selector, name: self, object: object)
	}
	func removeObserver(_ observer: Any, object: Any? = nil, from notificationCenter: NotificationCenter = .default) {
		notificationCenter.removeObserver(observer, name: self, object: object)
	}
	func post(with notificationCenter: NotificationCenter = .default, object: Any? = nil, userInfo: [AnyHashable: Any]? = nil) {
		notificationCenter.post(name: self, object: object, userInfo: nil)
	}
}

extension KeyedEncodingContainerProtocol {
	mutating func encode(_ value: Any, forKey key: Key) throws {
		switch value {
		case let value as String:
			try encode(value, forKey: key)
		case let collection as [Any]:
			var container = nestedUnkeyedContainer(forKey: key)
			for value in collection {
				try container.encode(value)
			}
		case let object as [String:Any]:
			var container = nestedContainer(keyedBy: String.self, forKey: key)
			for (key, value) in object {
				try container.encode(value, forKey: key)
			}
		default:
			throw EncodingError.invalidValue(
				value,
				.init(
					codingPath: codingPath,
					debugDescription: "Unable to encode value to JSON."
				)
			)
		}
	}
}

extension String: CodingKey {
	
	public var stringValue: String {
		return self
	}
	public var intValue: Int? {
		return nil
	}

	public init?(stringValue: String) {
		self = stringValue
	}
	public init?(intValue: Int) {
		return nil
	}
}

extension UnkeyedEncodingContainer {
	mutating func encode(_ value: Any) throws {
		switch value {
		case let value as String:
			try encode(value)
		case let collection as [Any]:
			var container = nestedUnkeyedContainer()
			for value in collection {
				try container.encode(value)
			}
		case let object as [String:Any]:
			var container = nestedContainer(keyedBy: String.self)
			for (key, value) in object {
				try container.encode(value, forKey: key)
			}
		default:
			throw EncodingError.invalidValue(
				value,
				.init(
					codingPath: codingPath,
					debugDescription: "Unable to encode value to JSON."
				)
			)
		}
	}
}

extension Data {
	init(_ bytes: UInt8...) {
		self.init(bytes)
	}

	var hexEncoded: String {
		map { String(format: "%02hhx", $0) }.joined()
	}
	
	func to<T>(type: T.Type) -> T? where T: ExpressibleByIntegerLiteral {
        var value: T = 0
        guard count >= MemoryLayout.size(ofValue: value) else { return nil }
		_ = Swift.withUnsafeMutableBytes(of: &value) { copyBytes(to: $0) }
		return value
    }
}
/*
struct AnyEncodable: Encodable {
	
	typealias Body = (Encoder) throws -> Void
	
	private let body: Body
	
	init(_ body: @escaping Body) {
		self.body = body
	}
	init<T: Encodable>(_ base: T) {
		self.init(base.encode)
	}

	func encode(to encoder: Encoder) throws {
		try body(encoder)
	}
}
*/
struct AnyEncodable: Encodable {

	private let base: Encodable
	
	init<T: Encodable>(_ base: T) {
		self.base = base
	}

	func encode(to encoder: Encoder) throws {
		try base.encode(to: encoder)
	}
}
