// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit
import AVFoundation

protocol QRCodeScannerViewControllerDelegate: class {
	func scannerViewController(_ scannerViewController: QRCodeScannerViewController, didScan stringValue: String)
}

class QRCodeScannerViewController: UIViewController {
	
	weak var delegate: QRCodeScannerViewControllerDelegate?
	
	// TODO: check access
	let session = AVCaptureSession()
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		guard let captureDevice = AVCaptureDevice.default(for: .video) else { return }
		// no camera
		do {
			let input = try AVCaptureDeviceInput(device: captureDevice)
			if session.canAddInput(input) {
				session.addInput(input)
			}
		} catch {
			let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
			let okButton = UIAlertAction(title: "Ok", style: .default) { [weak self] _ in
				self?.dismiss(animated: true)
			}
			alert.addAction(okButton)
			DispatchQueue.main.async {
				self.present(alert, animated: true, completion: nil)
			}
			return
		}
		
		let output = AVCaptureMetadataOutput()
		if session.canAddOutput(output) {
			session.addOutput(output)
			output.setMetadataObjectsDelegate(self, queue: .main)
			output.metadataObjectTypes = [.qr]
		}
		
		let videoLayer = AVCaptureVideoPreviewLayer(session: session)
		videoLayer.videoGravity = .resizeAspectFill
		videoLayer.frame = view.layer.bounds
		view.layer.addSublayer(videoLayer)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		session.startRunning()
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		session.stopRunning()
	}

}

extension QRCodeScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
	func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
		guard let metadataObject = metadataObjects.first else { return }
		guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
		guard let stringValue = readableObject.stringValue else { return }
		// TODO: sound/vibration effect
		delegate?.scannerViewController(self, didScan: stringValue)
		dismiss(animated: true)
	}
}
