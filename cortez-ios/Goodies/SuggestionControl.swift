// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

@objc protocol SuggestionControlDataSource: class {
	func suggestionControl(_ suggestionControl: SuggestionControl, suggestionsForPrefix prefix: String) -> [String]
}

@objc protocol SuggestionControlDelegate: class {
	func suggestionControlTextViewDidChange(_ suggestionControl: SuggestionControl)
}

class SuggestionControl: NSObject {

	@IBOutlet weak var textView: UITextView? {
		willSet {
			textView?.delegate = nil
		}
		didSet {
			textView?.delegate = self
		}
	}
	@IBOutlet weak var suggestionToolbar: SuggestionToolbar? {
		willSet {
			suggestionToolbar?.suggestionDelegate = nil
		}
		didSet {
			suggestionToolbar?.suggestionDelegate = self
		}
	}
	
	@IBOutlet weak var dataSource: SuggestionControlDataSource?
	@IBOutlet weak var delegate: SuggestionControlDelegate?

}

extension SuggestionControl: UITextViewDelegate {
	func textViewDidChangeSelection(_ textView: UITextView) {
		defer { delegate?.suggestionControlTextViewDidChange(self) }
		guard let dataSource = dataSource else { return }
		guard let suggestionToolbar = suggestionToolbar else { return }

		// extract prefix
		guard let prefix = textView.prefix, !prefix.isEmpty else {
			suggestionToolbar.suggestions = [] // no prefix found clear everything
			return
		}

		// ask delegate for suggestions
		let suggestions = dataSource.suggestionControl(self, suggestionsForPrefix: prefix)

		// update suggestionsView otherwise
		suggestionToolbar.suggestions = suggestions
	}
	func textViewDidEndEditing(_ textView: UITextView) {
		delegate?.suggestionControlTextViewDidChange(self)
	}
}

extension SuggestionControl: SuggestionToolbarDelegate {
	func suggestionToolbar(_ suggestionToolbar: SuggestionToolbar, didSelectSuggestion suggestion: String) {
		guard let textView = textView else { return }
		guard let text = textView.text else { return }
		guard let wordRange = textView.wordRange else { return }
		textView.replace(wordRange, withText: "\(suggestion)\(wordRange.upperBound == text.endIndex ? " " : "")")
	}
}
