// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

@IBDesignable
class Label: UILabel {
	
	@IBInspectable
	var borderColor: UIColor? {
		get { return layer.borderColor.map(UIColor.init) }
		set { layer.borderColor = newValue?.cgColor }
	}

	var textInsets = UIEdgeInsets.zero {
		didSet { invalidateIntrinsicContentSize() }
	}
	
	@IBInspectable
	var textTopInset: CGFloat {
		get { return textInsets.top }
		set { textInsets.top = newValue }
	}
	@IBInspectable
	var textLeftInset: CGFloat {
		get { return textInsets.left }
		set { textInsets.left = newValue }
	}
	@IBInspectable
	var textBottomInset: CGFloat {
		get { return textInsets.bottom }
		set { textInsets.bottom = newValue }
	}
	@IBInspectable
	var textRightInset: CGFloat {
		get { return textInsets.right }
		set { textInsets.right = newValue }
	}
	
	override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
		let insetRect = bounds.inset(by: textInsets)
		let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
		let invertedInsets = UIEdgeInsets(
			top: -textInsets.top,
			left: -textInsets.left,
			bottom: -textInsets.bottom,
			right: -textInsets.right
		)
		return textRect.inset(by: invertedInsets)
	}

	override func drawText(in rect: CGRect) {
		var newRect = rect
		switch contentMode {
		case .top:
			newRect.size.height = sizeThatFits(rect.size).height
		case .bottom:
			let height = sizeThatFits(rect.size).height
			newRect.origin.y += rect.size.height - height
			newRect.size.height = height
		default:
			break
		}
		super.drawText(in: newRect.inset(by: textInsets))
	}
}
