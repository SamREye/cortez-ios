// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit
import KeychainAccess

class ListSettingsViewController: UIViewController {

    override func viewDidLoad() {
		super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

	@IBAction func logout() {
		let alert = UIAlertController(title: nil, message: "Please make sure you still have your 24 words list on a piece of paper or your data will be lost forever.", preferredStyle: .alert)
		let okButton = UIAlertAction(title: "Ok", style: .default) { [weak self] _ in
			// TODO: delete Main Account contact
			let keychain = Keychain()
			try? keychain.remove("backup-phrase")
			try? keychain.remove("public-key")
			try? keychain.remove("secret-key")
			try? keychain.remove("biometry-secret-key")
			let provider = Provider.shared
			if let store: AnyStore<Operation> = provider.get() {
				store.deleteAll()
			}
			if let store: AnyStore<Account> = provider.get() {
				store.deleteAll()
			}
			NotificationCenter.default.post(name: Cortez.userDidLogout, object: self)
		}
		let cancelButton = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
		alert.addAction(okButton)
		alert.addAction(cancelButton)
		present(alert, animated: true, completion: nil)
	}
}
