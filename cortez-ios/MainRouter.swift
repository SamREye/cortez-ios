// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit
import KeychainAccess

protocol MainRoutingLogic {

	var sections: [UIViewController] { get }

	func routeToCreateWallet(with segue: UIStoryboardSegue?)
	func routeToRestoreWallet(with segue: UIStoryboardSegue?)
}

protocol MainDataPassingLogic {
	var dataStore: MainDataStore? { get }
}

final class MainRouter: NSObject, MainRoutingLogic, MainDataPassingLogic {

	var dataStore: MainDataStore?

	var sections: [UIViewController] = [
		"Transfer",
		"ListOperations",
		"ListAccounts",
		"ListSettings"
	].compactMap {
		UIStoryboard(name: $0, bundle: nil).instantiateInitialViewController()
	}

	weak var viewController: MainViewController?

	override init() {
		super.init()
		passDataToSections()
		Cortez.userDidLogin.addObserver(self, selector: #selector(reloadSections))
		Cortez.userDidLogout.addObserver(self, selector: #selector(reloadSections))
	}
	deinit {
		Cortez.userDidLogin.removeObserver(self)
		Cortez.userDidLogout.removeObserver(self)
	}

	func routeToCreateWallet(with segue: UIStoryboardSegue?) {
		guard let onboardController = segue?.destination as? OnboardController else { return }
		onboardController.router = CreateWalletRouter(.create)
	}
	func routeToRestoreWallet(with segue: UIStoryboardSegue?) {
		guard let onboardController = segue?.destination as? OnboardController else { return }
		onboardController.router = CreateWalletRouter(.restore) // TODO: restore
	}

	@objc private func reloadSections(_ notification: Notification) {
		passDataToSections()
	}
	
	private func passDataToSections() {
		dataStore?.source = try? Keychain().getData("public-key").flatMap(Source.init)
		sections.forEach(passDataToSection)
	}
	
	private func passDataToSection(_ section: UIViewController) {
		guard let dataStore = dataStore else { return }
		switch section {
		case let destination as TransferViewController:
			guard var destinationDataStore = destination.router?.dataStore else { break }
			passData(from: dataStore, to: &destinationDataStore)
		case let destination as ListOperationsViewController:
			guard var destinationDataStore = destination.router?.dataStore else { break }
			passData(from: dataStore, to: &destinationDataStore)
		case let destination as ListAccountsViewController:
			guard var destinationDataStore = destination.router?.dataStore as? ListAccountsDataStore else { break }
			passData(from: dataStore, to: &destinationDataStore)
		default:
			break
		}
	}

	func passData(from source: MainDataStore, to destination: inout TransferDataStore) {
		// ignore
	}
	func passData(from source: MainDataStore, to destination: inout ListOperationsDataStore) {
		destination.accountQuery = Query.where(.all)
		destination.operationQuery = Query.where(.all)
	}
	func passData(from source: MainDataStore, to destination: inout ListAccountsDataStore) {
		destination.address = source.source?.address
	}
}
