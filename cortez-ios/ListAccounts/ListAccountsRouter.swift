// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit
import KeychainAccess

protocol ListAccountsRoutingLogic: OnboardPageRoutingLogic {
	func routeToCreateAccount(with segue: UIStoryboardSegue?)
}

final class ListAccountsRouter: ListAccountsRoutingLogic, OnboardPageDataPassingLogic {

	var dataStore: OnboardPageDataStore?
	
	weak var viewController: ListAccountsViewController?

	func routeToCreateAccount(with segue: UIStoryboardSegue?) {
		guard let onboardController = segue?.destination as? OnboardController else { return }
		onboardController.router = CreateAccountRouter()
	}
	func routeToNextPage(with segue: UIStoryboardSegue?) {
		if let onboardController = viewController?.onboardController {
			onboardController.router?.routeToNextPage(with: nil)
		} else {
			guard let source = viewController else { return }
			guard let sourceDataStore = dataStore as? ListAccountsDataStore else { return }
			guard let destination = ListOperationsViewController.instantiateFromStoryboard() else { return }
			guard var destinationDataStore = destination.router?.dataStore else { return }
			pass(from: sourceDataStore, to: &destinationDataStore)
			navigate(from: source, to: destination)
		}
	}
	
	func passData(from source: OnboardPageDataStore, to destination: inout OnboardDataStore) {
		guard let source = source as? ListAccountsDataStore else { return }
		guard var destination = destination as? PayDataStore else { return }
		destination.source = source.selectedAccount
	}
	func pass(from source: ListAccountsDataStore, to destination: inout ListOperationsDataStore) {
		guard let address = source.selectedAccount?.address else { return }
		guard let manager = try? Keychain().getData("public-key").flatMap(Source.init) else { return }
		destination.isDelegatable = address != manager.address
		destination.accountQuery =  Query.where(.addressEquals(address))
		destination.operationQuery = Query.where(.sourceOrDestinationEquals(address))
	}

	func navigate(from source: ListAccountsViewController, to destination: ListOperationsViewController) {
		guard let presenter = viewController else { return }
		guard let viewer = ShowAccountViewController.instantiateFromStoryboard() else { return }
		viewer.pushViewController(destination, animated: false)
		presenter.present(viewer, animated: true, completion: nil)
	}
}
