// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

protocol ShowOperationPresentionLogic {
	func format(_ response: History.Operation.Detail.Response)
}

struct ShowOperationPresenter: ShowOperationPresentionLogic {

	weak var display: ShowOperationDisplayLogic?
	
	let dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateStyle = .long
		formatter.timeStyle = .long
		return formatter
	}()
	let tezFormatter: NumberFormatter = .tez()

	func format(_ response: History.Operation.Detail.Response) {
		let operation = response.operation
		let viewModel = History.Operation.Detail.ViewModel(
			rows: [
				.init(cell: .property, key: "hash", value: operation.operationHash),
				.init(cell: .property, key: "block hash", value: operation.blockHash),
				.init(cell: .property, key: "level", value: "\(operation.level)"),
				.init(cell: .property, key: "date", value: dateFormatter.string(from: operation.timestamp)),
				.address(key: "source", name: operation.sourceName, fallback: operation.source),
				.address(key: "destination", name: operation.destinationName, fallback: operation.destination),
				.init(cell: .tez, key: "amount", value: tezFormatter.string(for: Double(operation.amount).tez) ?? ""),
				.init(cell: .tez, key: "fee", value: tezFormatter.string(for: Double(operation.fee).tez) ?? "")
			]
		)
		display?.show(viewModel)
	}
}
