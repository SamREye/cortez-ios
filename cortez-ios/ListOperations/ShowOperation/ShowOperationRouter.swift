// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ShowOperationRoutingLogic {
	func routeToCreateContact(with segue: UIStoryboardSegue?)
}

protocol ShowOperationDataPassingLogic {
	var dataStore: ShowOperationDataStore? { get }
}

final class ShowOperationRouter: ShowOperationRoutingLogic, ShowOperationDataPassingLogic {
	
	var dataStore: ShowOperationDataStore?
	
	weak var viewController: ShowOperationViewController?
	
	func routeToCreateContact(with segue: UIStoryboardSegue?) {
		guard let source = viewController else { return }
		guard let dataStore = dataStore else { return }
		guard let destination = CreateContactViewController.instantiateFromStoryboard() else { return }
		guard var destinationDataStore = destination.router?.dataStore else { return }
		passData(from: dataStore, to: &destinationDataStore)
		navigate(from: source, to: destination)
	}

	func passData(from dataStore: ShowOperationDataStore, to destinationDataStore: inout CreateContactDataStore) {
		destinationDataStore.address = dataStore.selectedAddress
	}
	func navigate(from source: ShowOperationViewController, to destination: CreateContactViewController) {
		source.present(destination, animated: true, completion: nil)
	}
}
