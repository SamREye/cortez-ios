// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol OperationPropertyRow {
	var title: String? { get set }
	var value: String? { get set }
	var action: (() -> Void)? { get set }
}

extension OperationPropertyRow {
	var action: (() -> Void)? {
		get { return nil }
		set { }
	}
}

class OperationPropertyRowView: UITableViewCell, OperationPropertyRow {

	@IBOutlet weak var titleView: UILabel!
	@IBOutlet weak var valueView: UITextView!
	
	var title: String? {
		get { return titleView.text }
		set {
			titleView.text = newValue
			let size = bounds.size
			sizeThatFits(CGSize(width: size.width, height: .greatestFiniteMagnitude))
		}
	}
	var value: String? {
		get { return valueView.text }
		set { valueView.text = newValue }
	}
}

class OperationAddressRowView: UITableViewCell, OperationPropertyRow {
	
	@IBOutlet weak var titleView: UILabel!
	@IBOutlet weak var addressView: UITextView!
	
	override func awakeFromNib() {
		addressView.delegate = self
	}
	
	var title: String? {
		get { return titleView.text }
		set { titleView.text = newValue }
	}
	var value: String? {
		get { return addressView.text }
		set {
			guard let address = newValue else {
				addressView.attributedText = nil
				return
			}
			addressView.attributedText = NSMutableAttributedString(
				string: address,
				attributes: [
					.font: UIFont.systemFont(ofSize: 15),
					.link: address
				]
			)
		}
	}
	var action: (() -> Void)?
}

extension OperationAddressRowView: UITextViewDelegate {
	func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
		action?()
		return false
	}
}

class OperationContactRowView: UITableViewCell, OperationPropertyRow {
	
	@IBOutlet weak var titleView: UILabel!
	@IBOutlet weak var avatarView: UIImageView!
	@IBOutlet weak var nameView: UILabel!
	
	var title: String? {
		get { return titleView.text }
		set { titleView.text = newValue }
	}
	var value: String? {
		get { return nameView.text }
		set { nameView.text = newValue }
	}
}

class OperationTezRowView: UITableViewCell, OperationPropertyRow {
	
	@IBOutlet weak var titleView: UILabel!
	@IBOutlet weak var amountView: UITextView!
	
	var title: String? {
		get { return titleView.text }
		set { titleView.text = newValue }
	}
	var value: String? {
		get { return amountView.text }
		set { amountView.text = newValue }
	}
}
