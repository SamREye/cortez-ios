// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

protocol ShowOperationBusinessLogic {
	func execute(_ request: History.Operation.Detail.Request)
}

protocol ShowOperationDataStore {
	var operation: Operation? { get set }
	var selectedAddress: String? { get set }
}

final class ShowOperationInteractor: ShowOperationBusinessLogic, ShowOperationDataStore {

	var operation: Operation? {
		didSet {
			guard let operationHash = operation?.operationHash else { return }
			guard let store: AnyStore<Operation> = Provider.shared.get() else { return }
			selection = store.select(Query.where(.hashEquals(operationHash)).sorted(by: .operationHash, ascending: false))
			selection?.delegate = AnySelectionDelegate(self)
		}
	}
	var selectedAddress: String?

	let presenter: ShowOperationPresentionLogic
	
	private var selection: AnySelection<Operation>?

	init(presenter: ShowOperationPresentionLogic) {
		self.presenter = presenter
	}

	func execute(_ request: History.Operation.Detail.Request) {
		guard let operation = operation else { return }
		presenter.format(.init(operation: operation))
	}
}

extension ShowOperationInteractor: SelectionDelegate {
	func selection<T: Selection>(_ selection: T, didUpdate element: Operation, at indexPath: IndexPath) where Operation == T.SectionElement {
		operation = selection.first?.first
		guard let operation = operation else { return }
		presenter.format(.init(operation: operation))
	}
}
