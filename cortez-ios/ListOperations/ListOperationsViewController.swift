// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

enum ListOperationsSegue: String {
	case edit
	case select
}

protocol ListOperationsDisplayLogic: class {
	func show(_ viewModel: History.ViewModel)
	func show(_ viewModel: History.Account.ViewModel)
	func show(_ viewModel: History.Operation.List.ViewModel)
}

class ListOperationsViewController: UIViewController {

	var router: (ListOperationsRoutingLogic & ListOperationsDataPassingLogic)?
	var interactor: ListOperationsBusinessLogic?

	@IBOutlet private weak var balanceLabel: UILabel!
	@IBOutlet private weak var delegateLabel: UILabel!
	@IBOutlet private weak var delegateRow: UIView!
	@IBOutlet private weak var operationListView: UITableView!

	var balance: NSAttributedString? {
		didSet {
			guard isViewLoaded else { return }
			balanceLabel.attributedText = balance
		}
	}
	var delegate: String? {
		didSet {
			guard isViewLoaded else { return }
			delegateLabel.text = delegate
			delegateRow.isHidden = delegate == nil
		}
	}
	
	// TODO: AnySelection<Wallet.Account.ViewModel>?
	var selection: AnySelection<History.Operation.List.ViewModel.Row>?

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}

	private func setup() {
		let router = ListOperationsRouter()
		let interactor = ListOperationsInteractor(
			contractsRetriever: ContractsRetriever(),
			historyRetriever: HistoryRetriever(),
			presenter: ListOperationsPresenter(display: self)
		)
		router.dataStore = interactor
		router.viewController = self
		self.router = router
		self.interactor = interactor
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		if !(interactor?.isDelegatable ?? false) {
			navigationItem.rightBarButtonItem = nil
		}
	
		let refreshControl = UIRefreshControl()
		refreshControl.tintColor = .white
		refreshControl.addTarget(self, action: #selector(load), for: .valueChanged)
		operationListView.refreshControl = refreshControl
		operationListView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.size.height)
		operationListView.contentOffset = .zero
		
		balanceLabel.attributedText = balance
		delegateLabel.text = delegate
		delegateRow.isHidden = delegate == nil
		
		setSelectionDelegate()
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		load()
	}

	@objc func load() {
		interactor?.execute(.init())
	}
	
	private func setSelectionDelegate() {
		selection?.delegate = AnySelectionDelegate(SelectionTableViewControl(tableView: operationListView))
	}

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
		guard let destination = segue.identifier.flatMap(ListOperationsSegue.init) else { return }
		switch destination {
		case .edit:
			router?.routeToAccountEdition(with: segue)
		case .select:
			guard let indexPath = operationListView.indexPathForSelectedRow else { return }
			interactor?.selectOperation(at: indexPath)
			router?.routeToSelectedOperation(with: segue)
		}
    }
	
	@IBAction func unwind(segue: UIStoryboardSegue) {
		//dismiss(animated: true, completion: nil)
	}
}

extension ListOperationsViewController: UITableViewDataSource {

	func numberOfSections(in tableView: UITableView) -> Int {
		return selection?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return selection?[section].count ?? 0
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "operation", for: indexPath)
		if let row = selection?[indexPath], let cell = cell as? OperationRowView {
			cell.sourceLabel.text = row.source
			cell.destinationLabel.text = row.destination
			cell.amountLabel.text = row.amount
		}
		return cell
	}
}

extension ListOperationsViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 32
	}

	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let view = UIView(frame: .zero)
		view.backgroundColor = .clear

		let label = UILabel(frame: .zero)
		label.textColor = UIColor(named: "text")
		label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
		label.text = selection?[section].first?.date
		
		view.addSubview(label)

		label.translatesAutoresizingMaskIntoConstraints = false
		label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
		label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16).isActive = true
		label.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
		label.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
		
		return view
	}
}

extension ListOperationsViewController: ListOperationsDisplayLogic {

	func show(_ viewModel: History.ViewModel) {
		if let refreshControl = operationListView.refreshControl {
			refreshControl.endRefreshing()
		}
		guard let message = viewModel.message else { return }
		let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		let retryButton = UIAlertAction(title: "Retry", style: .default) { _ in
			self.load()
		}
		let okButton = UIAlertAction(title: "Ok", style: .default) { _ in }
		alert.addAction(retryButton)
		alert.addAction(okButton)
		present(alert, animated: true, completion: nil)
	}

	func show(_ viewModel: History.Account.ViewModel) {
		navigationItem.title = viewModel.name
		balance = viewModel.balance
		delegate = viewModel.delegate
	}

	func show(_ viewModel: History.Operation.List.ViewModel) {
		selection = viewModel.selection
		if isViewLoaded {
			setSelectionDelegate()
			operationListView.reloadData()
		}
	}
}

extension ListOperationsViewController {
	class func instantiateFromStoryboard() -> ListOperationsViewController? {
		let storyboard = UIStoryboard(name: "ListOperations", bundle: nil)
		return storyboard.instantiateInitialViewController() as? ListOperationsViewController
	}
}

final class OperationRowView: UITableViewCell {

	@IBOutlet weak var sourceLabel: UILabel!
	@IBOutlet weak var destinationLabel: UILabel!
	@IBOutlet weak var amountLabel: UILabel!
	
	var control: RippleTouchControl?
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setRippleControl()
	}
	
	func setRippleControl() {
		if let rippleView = contentView.subviews.first {
			control = RippleTouchControl(with: rippleView)
		}
		if let color = UIColor(named: "text")?.withAlphaComponent(0.5) {
			control?.rippleView?.rippleColor = color
		}
	}
}
