// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import UIKit

enum History {
	struct Request {}
	struct Response {
		let error: Error?
	}
	struct ViewModel {
		let message: String?
	}

	enum Account {}
	enum Operation {
		enum List {}
		enum Detail {}
	}
}

extension History.Account {
	struct Response {
		let name: String?
		let balance: UInt64?
		let delegate: String?
	}
	struct ViewModel {
		let name: String?
		let balance: NSAttributedString?
		let delegate: String?
	}
}

extension History.Operation.List {
	struct Response {
		let selection: AnySelection<Operation>?
	}
	struct ViewModel {
		let selection: AnySelection<Row>?
	}
}

extension History.Operation.Detail {
	struct Request {}
	struct Response {
		let operation: Operation
	}
	struct ViewModel {
		let rows: [Row]
	}
}

extension History.Operation.List.ViewModel {
	struct Row {
		let date: String? // section title (set only for the first row of each section)
		let source: String
		let destination: String
		let amount: String?
	}
}

extension History.Operation.Detail.ViewModel {
	struct Row {

		let cell: Cell
		let key: String
		let value: String
		
		static func address(key: String, name: String?, fallback: String) -> Row {
			guard let name = name else {
				return self.init(cell: .address, key: key, value: fallback)
			}
			return self.init(cell: .contact, key: key, value: name)
		}
	}
}

extension History.Operation.Detail.ViewModel.Row {
	enum Cell: String {
		case property
		case address
		case contact
		case tez
	}
}
