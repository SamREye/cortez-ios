// ***************************************************************************
//                                                                           
// Open Source License                                                       
// Copyright (c) 2018 Nomadic Development, Inc. <contact@tezcore.com>        
//                                                                           
// Permission is hereby granted, free of charge, to any person obtaining a   
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense,  
// and/or sell copies of the Software, and to permit persons to whom the     
// Software is furnished to do so, subject to the following conditions:      
//                                                                           
// The above copyright notice and this permission notice shall be included   
// in all copies or substantial portions of the Software.                    
//                                                                           
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       
// DEALINGS IN THE SOFTWARE.                                                 
//                                                                           
// ***************************************************************************

import Foundation

public typealias Completion<Value> = (Result<Value, Swift.Error>) -> Void

public protocol Completable {
	
	associatedtype Value
	
	func start(_ completion: @escaping Completion<Value>)
}

public extension Completable {
	func start() {
		start { _ in }
	}
}

public extension Completable {
	func await() throws -> Value {
		var result = Result<Value, Swift.Error>.failure(Error.uncomplete)
		let semaphore = DispatchSemaphore(value: 0)
		start {
			result = $0
			semaphore.signal()
		}
		semaphore.wait()
		switch result {
		case .success(let value):
			return value
		case .failure(let error):
			throw error
		}
	}
}

public struct AnyCompletable<Value>: Completable {
	
	public typealias Body = (@escaping Completion<Value>) -> Void
	
	private let body: Body

	public init(_ body: @escaping Body) {
		self.body = body
	}
	public init<T: Completable>(_ base: T) where Value == T.Value {
		self.init(base.start)
	}

	public func start(_ completion: @escaping Completion<Value>) {
		body(completion)
	}
}
